import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import First from './components/First.vue'
import Last from './components/Last.vue'
import Clothes from './components/first/Clothes.vue'
import Water from './components/first/Water.vue'
import Hat from './components/first/Hat.vue'

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes:[
    {path:'/', component:First,children:[
      {path:'/water',component:Water},
      {path:'/clothes',component:Clothes},
      {path:'/hat',component:Hat}
    ]},
    {path:'/last',component:Last,beforeEnter: (to, from, next) => {
      alert('页面施工中....')
      next(false);
    }}
  ]
})
