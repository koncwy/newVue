import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    commoditys:[
      {name:'北面高端UE保暖棉防水三合一女士冲锋衣',money:'￥4698',src:require('./assets/img/冲锋衣.jpg'),to:'/clothes'},
      {name:'浮生记书店Xforce out 瓜皮帽',money:'￥98',src:require('./assets/img/瓜皮帽.jpg'),to:'/hat'},
      {name:'黎水定制矿泉水',money:'￥699',src:require('./assets/img/黎水定制矿泉水.jpg'),to:'/water'}
    ],
    firstshow: true
  },
  mutations: {
    changeshow: state =>{
      state.firstshow = !state.firstshow;
    }
  },
  actions: {

  }
})
